import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger'

window.addEventListener('DOMContentLoaded', () => {
    // Handle close icon functionality
    searchForm();
    // Header Parallax
    headerParallax();
});

const headerParallax = () => {
    gsap.registerPlugin(ScrollTrigger);

    ScrollTrigger.create({
        start: 'top 1',
        end: 9999999,
        toggleActions: "restart none none reverse",
        toggleClass: {
            className: 'scrolled',
            targets: '.search-scroll--header'
        },
        onEnter: () => {
            gsap.to('.search-scroll--header h1', 0.1, {
                autoAlpha: 0,
                height: 0
            })
            gsap.to('.search-scroll--header-num-results', 0.1, {
                autoAlpha: 0,
                height: 0
            })
        },
        onLeaveBack: (e) => {
            gsap.to('.search-scroll--header h1', 0.1, {
                height: "auto",
                autoAlpha: 1
            })
            gsap.to('.search-scroll--header-num-results', 0.1, {
                height: "auto",
                autoAlpha: 1
            })
        }
    });

}

const searchForm = () => {
    const searchFormWrapper = document.querySelector('.search-scroll--header-form');
    if(searchFormWrapper){
        const searchInput = searchFormWrapper.querySelector('.form__field');
        if(searchInput.value !== "") {
            searchInput.parentNode.classList.add("focused");
        }
        searchInput.addEventListener("focusout", (e) => {
            if(e.target.value == "") {
                searchInput.parentNode.classList.remove("focused");
            }
        });
        searchInput.addEventListener("input", (e) => {
            if(e.target.value == "") {
                searchInput.parentNode.classList.remove("focused");
            } else {
                searchInput.parentNode.classList.add("focused");
            }
        });

        const closeButton = searchFormWrapper.querySelector('.close');
        closeButton.addEventListener("click", () => {
            searchInput.value = "";
            searchInput.parentNode.classList.remove("focused");
        })
    }
}