const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const del = require('del');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const cleancss = require('gulp-clean-css');
const browsersync = require('browser-sync').create();
const rollup = require('gulp-better-rollup');
const babel = require('rollup-plugin-babel');
const resolve = require('rollup-plugin-node-resolve');
const commonjs = require('rollup-plugin-commonjs');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');

function scssTask(){
    return src('src/assets/scss/main.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write())
        .pipe(cleancss())
        .pipe(dest('./src/assets/css/'))
}

function jsTask(){
    return src('src/assets/scripts/main.js')
        .pipe(rollup({ plugins: [babel(), resolve(), commonjs()] }, 'umd'))
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(dest('./src/assets/js/'));
}

function browsersyncServe(cb){
    browsersync.init({
        server: {
            baseDir: 'src'
        }    
    });
    cb();
}

function browsersyncReload(cb){
    browsersync.reload();
    cb();
}

// Watch Task
function watchTask(){
    watch('src/*.html', browsersyncReload);
    watch(['src/assets/scss/**/*.scss', 'src/assets/scripts/*.js'], series(scssTask, jsTask, browsersyncReload));
}

// Default Gulp Task
exports.default = series(
    scssTask,
    jsTask,
    browsersyncServe,
    watchTask
);